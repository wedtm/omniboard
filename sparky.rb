# require 'rubygems'
# require 'serialport'

# ser = SerialPort.new("/dev/tty.usbmodem4201", 115200, 8, 1, SerialPort::NONE)

require "pp"
require "serialport"

class TTy

	def initialize

		# defaults params for arduino serial
		baud_rate = 4800
		data_bits = 8
		stop_bits = 1
		parity = SerialPort::NONE

		# serial port
		@sp=nil
		@port=nil
	end
	
	def open port
		@sp = SerialPort.new(port, @baud_rate, @data_bits, @stop_bits, @parity)
	end
	
	
	def shutdown reason

		return if @sp==nil
		return if reason==:int

		printf("\nshutting down serial (%s)\n", reason)

		# you may write something before closing tty
		@sp.write(0x00)
		@sp.flush()
		printf("done\n")
	end
	
	def read
		@sp.flush()
		printf("# R : reading ...\n")
		c=nil
		while c==nil
			c=@sp.read(6)
			break if c != nil
		end
		return c
	end
	
	def write c
		@sp.putc(c)
		@sp.flush()
		printf("# W : 0x%02x\n", c.ord)
	end
	
	def flush
		@sp.flush
	end
end


# serial port should be connected to /dev/ttyUSB*
tty=TTy.new()
tty.open('/dev/tty.usbmodem4201')

at_exit     { tty.shutdown :exit }
trap("INT") { tty.shutdown :int  ; exit}

# # reading thread
# Thread.new do
# 	d=0x16
# 	pp d
# 	while true do

# 		sleep(0.01)
# 		tty.write(d)
# 		tty.flush()
		
# 		# build a loop with a value (d) cycling from 0 to 255
# 		d=d+1
# 		d=d%255
# 	end
# end

#just read forever
while true do
   while (true) do 
	  c=tty.read()
    end
end

sleep 500
tty.shutdown