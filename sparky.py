import serial
import time

ser = serial.Serial('/dev/tty.usbmodem3201', 4800, timeout=0,
                    parity=serial.PARITY_EVEN, rtscts=1)

while True:
  s = ser.read(100)       # read up to one hundred bytes
  print(s)
  time.sleep(0.5)
